Pipeline Details
================

Pipelines describe how Sources are ingested. A pipeline consists of a
collection of Pipes, each of which contains a series of steps which
define the order in which transformers are applied.

.. code-block:: json

    {
        "name": "pipeline_text",
        "pipes": [
            {
                "name": "plain",
                "sources": [
                    {
                        "type": "manifest",
                        "source": "text"
                    }
                ],
                "steps": [
                    {
                        "type": "text_source_create_document",
                        "arguments": {}
                    }
                ]
            }
        ]
    }

Running a pipeline from the command line
----------------------------------------

In ``tests/pipelines`` you'll find some example pipelines that you can
use as a starting point for designing your own pipelines.

To run a pipeline from the command line::

    $ ingestum-pipeline tests/pipelines/pipeline_twitter.json --search "sorcero"
    $ ingestum-pipeline tests/pipelines/pipeline_pdf.json --url file://tests/data/test.pdf --first-page 1 --last-page 3
    $ ingestum-pipeline tests/pipelines/pipeline_html.json --url file://tests/data/test.html --target body
    $ ingestum-pipeline tests/pipelines/pipeline_text.json --url file://tests/data/test.txt
    $ ingestum-pipeline tests/pipelines/pipeline_audio.json --url file://tests/data/test.wav
    $ ingestum-pipeline tests/pipelines/pipeline_csv.json --url file://tests/data/test.csv
    $ ingestum-pipeline tests/pipelines/pipeline_excel.json --url file://tests/data/test.xlsx
    $ ingestum-pipeline tests/pipelines/pipeline_image.json --url file://tests/data/test.jpg
    $ ingestum-pipeline tests/pipelines/pipeline_xml.json --url file://tests/data/test.xml

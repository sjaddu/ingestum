Documents Reference
===================

This is the reference page for documents implementation and format.

Document Base Class
-------------------

.. automodule:: ingestum.documents.base
   :members:
   :undoc-members:
   :show-inheritance:

Collection
----------

.. automodule:: ingestum.documents.collection
   :members:
   :undoc-members:
   :show-inheritance:

CSV
---

.. automodule:: ingestum.documents.csv
   :members:
   :undoc-members:
   :show-inheritance:

Form
----

.. automodule:: ingestum.documents.form
   :members:
   :undoc-members:
   :show-inheritance:

HTML
----

.. automodule:: ingestum.documents.html
   :members:
   :undoc-members:
   :show-inheritance:

Passage
-------

.. automodule:: ingestum.documents.passage
   :members:
   :undoc-members:
   :show-inheritance:

Resource
--------

.. automodule:: ingestum.documents.resource
   :members:
   :undoc-members:
   :show-inheritance:

Tabular
-------

.. automodule:: ingestum.documents.tabular
   :members:
   :undoc-members:
   :show-inheritance:

Text
----

.. automodule:: ingestum.documents.text
   :members:
   :undoc-members:
   :show-inheritance:

XML
---

.. automodule:: ingestum.documents.xml
   :members:
   :undoc-members:
   :show-inheritance:

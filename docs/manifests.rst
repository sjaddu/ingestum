Manifests Reference
===================

This is the reference page for manifests implementation and format.

Manifest Base Class
-------------------

.. automodule:: ingestum.manifests.base
   :members:
   :undoc-members:
   :show-inheritance:

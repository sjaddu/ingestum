Pipelines Reference
===================

This is the reference page for pipelines implementation and format.

Pipeline Base Class
-------------------

.. automodule:: ingestum.pipelines.base
   :members:
   :undoc-members:
   :show-inheritance:

Conditionals Reference
======================

This is the reference page for conditionals implementation and format.

Conditional Base Class
----------------------

.. automodule:: ingestum.conditionals.base
   :members:
   :undoc-members:
   :show-inheritance:

AllAnd
------

.. automodule:: ingestum.conditionals.all_and
   :members:
   :undoc-members:
   :show-inheritance:

AllAttributeMatchesRegexp
-------------------------

.. automodule:: ingestum.conditionals.all_attribute_matches_regexp
   :members:
   :undoc-members:
   :show-inheritance:

AllNegate
---------

.. automodule:: ingestum.conditionals.all_negate
   :members:
   :undoc-members:
   :show-inheritance:

AllOr
-----

.. automodule:: ingestum.conditionals.all_or
   :members:
   :undoc-members:
   :show-inheritance:

PassageHasContentPrefix
-----------------------

.. automodule:: ingestum.conditionals.passage_has_content_prefix
   :members:
   :undoc-members:
   :show-inheritance:

PassageHasTagPrefix
-------------------

.. automodule:: ingestum.conditionals.passage_has_tag_prefix
   :members:
   :undoc-members:
   :show-inheritance:

TabularHasFewerColumns
----------------------

.. automodule:: ingestum.conditionals.tabular_has_fewer_columns
   :members:
   :undoc-members:
   :show-inheritance:

TabularRowHasEmptyFrontCells
----------------------------

.. automodule:: ingestum.conditionals.tabular_row_has_empty_front_cells
   :members:
   :undoc-members:
   :show-inheritance:

TabularRowHasNValues
--------------------

.. automodule:: ingestum.conditionals.tabular_row_has_n_values
   :members:
   :undoc-members:
   :show-inheritance:

TabularRowMatches
-----------------

.. automodule:: ingestum.conditionals.tabular_row_matches
   :members:
   :undoc-members:
   :show-inheritance:

TabularRowMatchesRegexp
-----------------------

.. automodule:: ingestum.conditionals.tabular_row_matches_regexp
   :members:
   :undoc-members:
   :show-inheritance:

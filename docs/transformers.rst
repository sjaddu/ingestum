Transformers Reference
======================

This is the reference page for transformers implementation and format.

Transformer Base Class
----------------------

.. automodule:: ingestum.transformers.base
   :members:
   :undoc-members:
   :show-inheritance:

AudioSourceCreateTextDocument
-----------------------------

.. automodule:: ingestum.transformers.audio_source_create_text_document
   :members:
   :undoc-members:
   :show-inheritance:

CollectionDocumentAdd
---------------------

.. automodule:: ingestum.transformers.collection_document_add
   :members:
   :undoc-members:
   :show-inheritance:

CollectionDocumentJoin
----------------------

.. automodule:: ingestum.transformers.collection_document_join
   :members:
   :undoc-members:
   :show-inheritance:

CollectionDocumentMerge
-----------------------

.. automodule:: ingestum.transformers.collection_document_merge
   :members:
   :undoc-members:
   :show-inheritance:

CollectionDocumentRemoveOnConditional
-------------------------------------

.. automodule:: ingestum.transformers.collection_document_remove_on_conditional
   :members:
   :undoc-members:
   :show-inheritance:

CollectionDocumentTransform
---------------------------

.. automodule:: ingestum.transformers.collection_document_transform
   :members:
   :undoc-members:
   :show-inheritance:

CollectionDocumentTransformOnConditional
----------------------------------------

.. automodule:: ingestum.transformers.collection_document_transform_on_conditional
   :members:
   :undoc-members:
   :show-inheritance:

CSVDocumentCreateTabular
------------------------

.. automodule:: ingestum.transformers.csv_document_create_tabular
   :members:
   :undoc-members:
   :show-inheritance:

CSVSourceCreateDocument
-----------------------

.. automodule:: ingestum.transformers.csv_source_create_document
   :members:
   :undoc-members:
   :show-inheritance:

DocumentExtract
---------------

.. automodule:: ingestum.transformers.document_extract
   :members:
   :undoc-members:
   :show-inheritance:

DocumentSourceCreateDocument
----------------------------

.. automodule:: ingestum.transformers.document_source_create_document
   :members:
   :undoc-members:
   :show-inheritance:

EmailSourceCreateHTMLCollectionDocument
---------------------------------------

.. automodule:: ingestum.transformers.email_source_create_html_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

EmailSourceCreateTextCollectionDocument
---------------------------------------

.. automodule:: ingestum.transformers.email_source_create_text_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

HTMLDocumentImagesExtract
-------------------------

.. automodule:: ingestum.transformers.html_document_images_extract
   :members:
   :undoc-members:
   :show-inheritance:

HTMLDocumentSubReplaceForUnicode
--------------------------------

.. automodule:: ingestum.transformers.html_document_sub_replace_for_unicode
   :members:
   :undoc-members:
   :show-inheritance:

HTMLDocumentSupReplaceForUnicode
--------------------------------

.. automodule:: ingestum.transformers.html_document_sup_replace_for_unicode
   :members:
   :undoc-members:
   :show-inheritance:

HTMLSourceCreateDocument
------------------------

.. automodule:: ingestum.transformers.html_source_create_document
   :members:
   :undoc-members:
   :show-inheritance:

ImageSourceCreateReferenceTextDocument
--------------------------------------

.. automodule:: ingestum.transformers.image_source_create_reference_text_document
   :members:
   :undoc-members:
   :show-inheritance:

ImageSourceCreateTextDocument
-----------------------------

.. automodule:: ingestum.transformers.image_source_create_text_document
   :members:
   :undoc-members:
   :show-inheritance:

PassageDocumentAddMetadataFromMetadata
--------------------------------------

.. automodule:: ingestum.transformers.passage_document_add_metadata_from_metadata
   :members:
   :undoc-members:
   :show-inheritance:

PassageDocumentAddMetadata
--------------------------

.. automodule:: ingestum.transformers.passage_document_add_metadata
   :members:
   :undoc-members:
   :show-inheritance:

PassageDocumentAddMetadataOnAttribute
-------------------------------------

.. automodule:: ingestum.transformers.passage_document_add_metadata_on_attribute
   :members:
   :undoc-members:
   :show-inheritance:

PassageDocumentStringSplit
--------------------------

.. automodule:: ingestum.transformers.passage_document_string_split
   :members:
   :undoc-members:
   :show-inheritance:

PassageDocumentTransformOnConditional
-------------------------------------

.. automodule:: ingestum.transformers.passage_document_transform_on_conditional
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCreateFormDocument
---------------------------

.. automodule:: ingestum.transformers.pdf_source_create_form_document
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCreateTabularCollectionDocument
----------------------------------------

.. automodule:: ingestum.transformers.pdf_source_create_tabular_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCreateTextDocument
---------------------------

.. automodule:: ingestum.transformers.pdf_source_create_text_document
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCreateTextDocumentOCR
------------------------------

.. automodule:: ingestum.transformers.pdf_source_create_text_document_ocr
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCreateTextDocumentReplacedExtractables
-----------------------------------------------

.. automodule:: ingestum.transformers.pdf_source_create_text_document_replaced_extractables
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCropCreateImageSource
------------------------------

.. automodule:: ingestum.transformers.pdf_source_crop_create_image_source
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceCropExtract
--------------------

.. automodule:: ingestum.transformers.pdf_source_crop_extract
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceImagesCreateResourceCollectionDocument
-----------------------------------------------

.. automodule:: ingestum.transformers.pdf_source_images_create_resource_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceImagesExtract
----------------------

.. automodule:: ingestum.transformers.pdf_source_images_extract
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceShapesCreateResourceCollectionDocument
-----------------------------------------------

.. automodule:: ingestum.transformers.pdf_source_shapes_create_resource_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceShapesExtract
----------------------

.. automodule:: ingestum.transformers.pdf_source_shapes_extract
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceTablesExtract
----------------------

.. automodule:: ingestum.transformers.pdf_source_tables_extract
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceTextCreateTextCollectionDocument
-----------------------------------------

.. automodule:: ingestum.transformers.pdf_source_text_create_text_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

PDFSourceTextExtract
--------------------

.. automodule:: ingestum.transformers.pdf_source_text_extract
   :members:
   :undoc-members:
   :show-inheritance:

ProquestSourceCreateXMLCollectionDocument
-----------------------------------------

.. automodule:: ingestum.transformers.proquest_source_create_xml_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

PubmedSourceCreateXMLCollectionDocument
-----------------------------------------

.. automodule:: ingestum.transformers.pubmed_source_create_xml_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

ResourceCreateTextDocument
--------------------------

.. automodule:: ingestum.transformers.resource_create_text_document
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentCellTransposeOnConditional
-----------------------------------------

.. automodule:: ingestum.transformers.tabular_document_cell_transpose_on_conditional
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentColumnsInsert
----------------------------

.. automodule:: ingestum.transformers.tabular_document_columns_insert
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentColumnsStringReplace
-----------------------------------

.. automodule:: ingestum.transformers.tabular_document_columns_string_replace
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentColumnsUpdateWithExtractables
--------------------------------------------

.. automodule:: ingestum.transformers.tabular_document_columns_update_with_extractables
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentCreateFormCollection
-----------------------------------

.. automodule:: ingestum.transformers.tabular_document_create_form_collection
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentCreateFormCollectionWithHeaders
----------------------------------------------

.. automodule:: ingestum.transformers.tabular_document_create_form_collection_with_headers
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentCreateMDPassage
------------------------------

.. automodule:: ingestum.transformers.tabular_document_create_md_passage
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentFit
------------------

.. automodule:: ingestum.transformers.tabular_document_fit
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentJoin
-------------------

.. automodule:: ingestum.transformers.tabular_document_join
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentRowMergeOnConditional
------------------------------------

.. automodule:: ingestum.transformers.tabular_document_row_merge_on_conditional
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentRowRemoveOnConditional
-------------------------------------

.. automodule:: ingestum.transformers.tabular_document_row_remove_on_conditional
   :members:
   :undoc-members:
   :show-inheritance:

TabularDocumentStripUntilConditional
------------------------------------

.. automodule:: ingestum.transformers.tabular_document_strip_until_conditional
   :members:
   :undoc-members:
   :show-inheritance:

TextCreatePassageDocument
-------------------------

.. automodule:: ingestum.transformers.text_create_passage_document
   :members:
   :undoc-members:
   :show-inheritance:

TextDocumentAddPassageMarker
----------------------------

.. automodule:: ingestum.transformers.text_document_add_passage_marker
   :members:
   :undoc-members:
   :show-inheritance:

TextDocumentHyphensRemove
-------------------------

.. automodule:: ingestum.transformers.text_document_hyphens_remove
   :members:
   :undoc-members:
   :show-inheritance:

TextDocumentJoin
----------------

.. automodule:: ingestum.transformers.text_document_join
   :members:
   :undoc-members:
   :show-inheritance:

TextDocumentStringReplace
-------------------------

.. automodule:: ingestum.transformers.text_document_string_replace
   :members:
   :undoc-members:
   :show-inheritance:

TextSourceCreateDocument
------------------------

.. automodule:: ingestum.transformers.text_source_create_document
   :members:
   :undoc-members:
   :show-inheritance:

TextSplitiIntoCollectionDocument
--------------------------------

.. automodule:: ingestum.transformers.text_split_into_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

TwitterSourceCreateFormCollectionDocument
-----------------------------------------

.. automodule:: ingestum.transformers.twitter_source_create_form_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

XLSSourceCreateCSVCollectionDocument
------------------------------------

.. automodule:: ingestum.transformers.xls_source_create_csv_collection_document
   :members:
   :undoc-members:
   :show-inheritance:

XLSSourceCreateCSVDocument
--------------------------

.. automodule:: ingestum.transformers.xls_source_create_csv_document
   :members:
   :undoc-members:
   :show-inheritance:

XMLCreateTextDocument
---------------------

.. automodule:: ingestum.transformers.xml_create_text_document
   :members:
   :undoc-members:
   :show-inheritance:

XMLDocumentTagReplace
---------------------

.. automodule:: ingestum.transformers.xml_document_tag_replace
   :members:
   :undoc-members:
   :show-inheritance:

XMLSourceCreateDocument
-----------------------

.. automodule:: ingestum.transformers.xml_source_create_document
   :members:
   :undoc-members:
   :show-inheritance:

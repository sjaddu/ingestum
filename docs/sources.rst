Sources Reference
=================

This is the reference page for sources implementation and format.

Source Base Class
-----------------

.. automodule:: ingestum.sources.base
   :members:
   :undoc-members:
   :show-inheritance:

Audio
-----

.. automodule:: ingestum.sources.audio
   :members:
   :undoc-members:
   :show-inheritance:

CSV
---

.. automodule:: ingestum.sources.csv
   :members:
   :undoc-members:
   :show-inheritance:

Email
-----

.. automodule:: ingestum.sources.email
   :members:
   :undoc-members:
   :show-inheritance:

HTML
----

.. automodule:: ingestum.sources.html
   :members:
   :undoc-members:
   :show-inheritance:

Image
-----

.. automodule:: ingestum.sources.image
   :members:
   :undoc-members:
   :show-inheritance:

PDF
---

.. automodule:: ingestum.sources.pdf
   :members:
   :undoc-members:
   :show-inheritance:

ProQuest
--------

.. automodule:: ingestum.sources.proquest
   :members:
   :undoc-members:
   :show-inheritance:

PubMed
--------

.. automodule:: ingestum.sources.pubmed
   :members:
   :undoc-members:
   :show-inheritance:

Text
----

.. automodule:: ingestum.sources.text
   :members:
   :undoc-members:
   :show-inheritance:

Twitter
-------

.. automodule:: ingestum.sources.twitter
   :members:
   :undoc-members:
   :show-inheritance:

XLS
---

.. automodule:: ingestum.sources.xls
   :members:
   :undoc-members:
   :show-inheritance:

XML
---

.. automodule:: ingestum.sources.xml
   :members:
   :undoc-members:
   :show-inheritance:
